module gitlab.com/serebit/ypkg2-tools

go 1.17

require (
	github.com/DataDrake/cli-ng/v2 v2.0.2
	github.com/DataDrake/waterlog v1.2.0
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/DataDrake/flair v0.5.1 // indirect
