package cli

import (
	"fmt"
	"github.com/DataDrake/cli-ng/v2/cmd"
	"github.com/DataDrake/waterlog"
	"gitlab.com/serebit/ypkg2-tools/core"
	"gitlab.com/serebit/ypkg2-tools/core/checks"
)

var check = &cmd.Sub{
	Name:  "check",
	Short: "Validate the contents of a package.yml buildscript",
	Alias: "c",
	Run: func(_ *cmd.Root, _ *cmd.Sub) {
		y, err := core.UnmarshalYaml("package.yml")

		if err != nil {
			waterlog.Fatalln(err)
		}

		for _, c := range checks.Checks {
			result := c.Task(y)

			status := "\033[32;1mPASS:"
			if result.Status == checks.CheckSkip {
				status = "\033[2mSKIP:"
			} else if result.Status == checks.CheckFail {
				status = "\033[31;1mFAIL:"
			}

			fmt.Println(status, c.Desc, "\033[0m")

			for _, it := range result.Errs {
				fmt.Printf("\t\033[31m- %s\033[0m\n", it)
			}
		}
	},
}
