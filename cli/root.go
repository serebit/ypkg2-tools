package cli

import (
	"github.com/DataDrake/cli-ng/v2/cmd"
	"github.com/DataDrake/waterlog"
	"github.com/DataDrake/waterlog/format"
	"github.com/DataDrake/waterlog/level"
)

type GlobalFlags struct{}

var Root = &cmd.Root{
	Name:  "ypkg2-tools",
	Short: "A community-made toolset to aid with Solus package development",
	Flags: &GlobalFlags{},
}

func init() {
	cmd.Register(&cmd.Help)
	cmd.Register(version)
	cmd.Register(check)

	waterlog.SetLevel(level.Info)
	waterlog.SetFormat(format.Min)
}
