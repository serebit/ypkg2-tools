package cli

import (
	"github.com/DataDrake/cli-ng/v2/cmd"
	"github.com/DataDrake/waterlog"
)

var VersionCode string // defined by ldflags

var version = &cmd.Sub{
	Name:  "version",
	Short: "Print the version and exit",
	Alias: "v",
	Run: func(_ *cmd.Root, _ *cmd.Sub) {
		waterlog.Printf("ypkg2-tools version %v\n", VersionCode)
	},
}
