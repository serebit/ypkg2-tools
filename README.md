# ypkg2-tools

A community-developed toolset for Solus package development with ypkg2.

## Disclaimer

This project is a **community** effort, and Will be superseded by built-in tools provided with ypkg3. **This project is
not officially endorsed by Solus.**

## License

ypkg2-tools is open-sourced under
the [Creative Commons Zero v1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) license. This
license dedicates all code submitted to the public domain. All contributions to this repository are subject to this
license.
