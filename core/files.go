package core

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

func ReadFile(path string) ([]byte, error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return []byte{}, err
	}

	return ioutil.ReadFile(filepath.Clean(path))
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}
