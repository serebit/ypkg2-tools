package checks

import (
	"fmt"
	"gitlab.com/serebit/ypkg2-tools/core"
	"os/exec"
)

var homepage = Check{
	Desc: "Homepage should be accessible and support SSL",
	Task: func(y *core.Yaml) (result CheckResult) {
		if !core.Contains(y.Keys, "homepage") {
			return skip()
		}

		curl := exec.Command("curl", y.Map["homepage"].(string))
		err := curl.Run()
		if err != nil {
			return fail(fmt.Errorf("curl failed to connect to the homepage: %s", err))
		}

		return result
	},
}
