package checks

import (
	"fmt"
	"gitlab.com/serebit/ypkg2-tools/core"
)

var orderedKeys = []string{
	"name", "version", "release",
	"source",
	"license", "component",
	"homepage", "summary", "description",
	"emul32", "networking", "clang",
	"patterns", "replaces",
	"builddeps", "rundeps",
	"setup", "build", "install", "check", "profile"}

var order = Check{
	Desc: "Keys should be in the correct order",
	Task: func(y *core.Yaml) (result CheckResult) {
		var presentKeys []string
		for _, it := range orderedKeys {
			if y.Map[it] != nil {
				presentKeys = append(presentKeys, it)
			}
		}

		var intersection []string
		for _, it := range y.Slice {
			if core.Contains(orderedKeys, it.Key.(string)) {
				intersection = append(intersection, it.Key.(string))
			}
		}

		var inWrongPlace []string
		for i := range intersection {
			if intersection[i] != presentKeys[i] {
				inWrongPlace = append(inWrongPlace, intersection[i])
			}
		}

		if len(inWrongPlace) != 0 {
			result = fail(
				fmt.Errorf("following elements are in the wrong place: %v", inWrongPlace),
				fmt.Errorf("current order: %v", intersection),
				fmt.Errorf("correct order: %v", presentKeys),
			)
		}

		return result
	},
}
