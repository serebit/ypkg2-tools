package core

func Contains(elements []string, a string) bool {
	for _, it := range elements {
		if a == it {
			return true
		}
	}

	return false
}
